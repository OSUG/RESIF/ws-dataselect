FROM gcc as dataselect
WORKDIR /src
RUN wget https://codeload.github.com/iris-edu/dataselect/tar.gz/v3.22
RUN tar xf v3.22
WORKDIR /src/dataselect-3.22/
RUN make

FROM tomcat:8.5.87
RUN apt-get update && apt-get install -y jq python3 python3-pip python3-dateutil postgresql-client libxml2-utils redis-tools && apt-get clean
RUN python3 -m pip install pytz
# Tomcat authentication config
COPY deps/server.xml /usr/local/tomcat/conf/server.xml
# IRIS WSS
COPY deps/webserviceshell-2.4.9.war /usr/local/tomcat/webapps/fdsnws#dataselect#1.war
COPY deps/postgresql-42.2.14.jar /usr/local/tomcat/lib/
COPY deps/server.xml /usr/local/tomcat/conf
ENV JAVA_OPTS="-DwssConfigDir='/home/sysop/wss-config' -Dwss.digest.realmname='FDSN'"
RUN mkdir -p /home/sysop/work/requests
RUN mkdir -p /home/sysop/logs
RUN chown -R 1500:1500 /home/sysop
COPY deps-git/wss-config /home/sysop/wss-config
# Compiled dataselect
COPY --from=dataselect /src/dataselect-3.22/dataselect /bin/dataselect
# Python handler
COPY deps-git/wss-python-handler/WSShandler.py /home/sysop/wss-python-handler/WSShandler-dataselect.py
COPY deps-git/wss-python-handler/ws_spec.py /home/sysop/wss-python-handler/
# Resif Backend
COPY deps-git/wss-resif-backend/ws_backend.py  deps-git/wss-resif-backend/ws_requestcache.py /home/sysop/wss-python-handler/
# Data extractor
COPY deps-git/ws-extract-scripts /home/sysop/ws-extract-scripts
RUN mkdir -p /mnt/auto/archive/data /mnt/auto/archive/rawdata/bud

EXPOSE 8080

RUN ln -s /home/sysop/logs /root/logs
CMD ["./bin/catalina.sh", "run"]
