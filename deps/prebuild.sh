# Get all dependencies before building Dockerfile
# wss
[ -f webserviceshell-2.4.9.war ] || curl -L -o deps/webserviceshell-2.4.9.war  https://github.com/iris-edu/webserviceshell/releases/download/v2.4.9/webserviceshell-2.4.9.war
# dataselect
[ -f v3.22 ] || curl -L -o deps/v3.22 https://codeload.github.com/iris-edu/dataselect/tar.gz/v3.22
# postgres jdbc
[ -f postgresql-42.2.14.jar ] || curl -L -o deps/postgresql-42.2.14.jar https://jdbc.postgresql.org/download/postgresql-42.2.14.jar
