# Webservice dataselect en container

Ce projet est un empaquetage pour exécuter le webservice dataselect sous forme de container.

Il dépend de plusieurs projets Git enregistrés en submodules.

Pour mettre à jour ces submodules, faire la manip suivante (dans cet exemple c'est wss-config)

    git submodule update --remote --merge
    git add deps-git/wss-config
    git commit -m "update wss-config submodule"
    git push

Le Dockerfile crée un container complet.

Le fichier .gitlab-ci.yaml permet de construire et héberger ce container sur gitlab.
